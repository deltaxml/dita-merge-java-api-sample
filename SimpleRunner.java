// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import com.deltaxml.ditamerge.ThreeWayMerge;
import com.deltaxml.ditamerge.ThreeWayMerge.ResultFormat;
import com.deltaxml.ditamerge.ThreeWayMerge.ThreeWayMergeResultType;
import com.deltaxml.ditamerge.ConcurrentMerge;
import com.deltaxml.ditamerge.SequentialMerge;
import com.deltaxml.ditamerge.ConcurrentMerge.MergeResultType;

import java.io.File;
import java.io.FileNotFoundException;

public class SimpleRunner {
  public static void main(String[] args) throws Exception {
    File dataFolder = new File("data/");

    // Ancestor file
    File ancestorFile = new File(dataFolder, "four-edits.xml");

    // Version files
    File annaVersion = new File(dataFolder, "four-edits-anna.xml");
    File benVersion = new File(dataFolder, "four-edits-ben.xml");
    File chrisVersion = new File(dataFolder, "four-edits-chris.xml");
    File davidVersion = new File(dataFolder, "four-edits-david.xml");

    // Result File
    File resultFile = new File("result.xml");

    System.out.println("Running Concurrent Merge....");
    ConcurrentMerge merger= new ConcurrentMerge();

    merger.setAncestor(ancestorFile, "master");
    System.out.println("Ancestor (\"master\") set");

    merger.addVersion(annaVersion, "anna");
    System.out.println("Version \"anna\" added");

    merger.addVersion(benVersion, "ben");
    System.out.println("Version \"ben\" added");

    merger.addVersion(chrisVersion, "chris");
    System.out.println("Version \"chris\" added");

    merger.addVersion(davidVersion, "david");
    System.out.println("Version \"david\" added");

    merger.setResultType(MergeResultType.DELTAV2);
    merger.extractAll(new File("merge-result-deltav2.xml"));
    merger.setResultType(MergeResultType.ANALYZED_DELTAV2);
    merger.extractAll(new File("merge-result-analyzed.xml"));
    merger.setResultType(MergeResultType.RULE_PROCESSED_DELTAV2);
    merger.extractAll(new File("merge-result-processed.xml"));
    merger.setResultType(MergeResultType.SIMPLIFIED_DELTAV2);
    merger.extractAll(new File("merge-result-simplified-delta.xml"));
    merger.setResultType(MergeResultType.SIMPLIFIED_RULE_PROCESSED_DELTAV2);
    merger.extractAll(new File("merge-result-simplified-rule-processed-delta.xml"));
    // We could also construct a RuleConfiguration object here and do more specific rule processing.
    
    // Now using the three way merger we will generate a variety of three way results.
    // The use of 'presets' is demonstrated in the 'ThreeToTwoMergeUseCases' sample
    System.out.println("Running Three Way Concurrent Merge....");
    ThreeWayMerge tw= new ThreeWayMerge();
    tw.setThreeWayResultType(ThreeWayMergeResultType.THREE_WAY_OXYGEN_TRACK_CHANGES);
    tw.merge(ancestorFile, annaVersion, benVersion, new File("result-three-way-oxygen-track-changes.xml"));
    
    tw.setThreeWayResultType(ThreeWayMergeResultType.TWO_WAY_RESULT);
    tw.setResultFormat(ResultFormat.XML_DELTA);
    tw.merge(ancestorFile, annaVersion, benVersion, new File("result-two-way-xml-delta.xml"));

    tw.setThreeWayResultType(ThreeWayMergeResultType.TWO_WAY_RESULT);
    tw.setResultFormat(ResultFormat.OXYGEN_TRACK_CHANGES);
    tw.merge(ancestorFile, annaVersion, benVersion, new File("result-two-way-oxygen-track-changes.xml"));

    tw.setThreeWayResultType(ThreeWayMergeResultType.RULE_PROCESSED_TWO_WAY_RESULT);
    tw.setResultFormat(ResultFormat.XML_DELTA);
    tw.merge(ancestorFile, annaVersion, benVersion, new File("result-rule-processed-two-way-xml-delta.xml"));

    tw.setThreeWayResultType(ThreeWayMergeResultType.TWO_WAY_RESULT);
    tw.setResultFormat(ResultFormat.OXYGEN_TRACK_CHANGES);
    tw.merge(ancestorFile, annaVersion, benVersion, new File("result-rule-processed-two-way-oxygen-track-changes.xml"));
    
    tw.setThreeWayResultType(ThreeWayMergeResultType.TWO_WAY_RESULT);
    tw.setResultFormat(ResultFormat.DITA_MARKUP);
    tw.merge(ancestorFile, annaVersion, benVersion, new File("result-two-way-dita-markup.xml"));

    tw.setThreeWayResultType(ThreeWayMergeResultType.RULE_PROCESSED_TWO_WAY_RESULT);
    tw.setResultFormat(ResultFormat.DITA_MARKUP);
    tw.merge(ancestorFile, annaVersion, benVersion, new File("result-rule-processed-two-way-dita-markup.xml"));

    System.out.println("Running Sequential Merge....");
    SequentialMerge seqMerger= new SequentialMerge();

    seqMerger.addVersion(ancestorFile, "master");
    System.out.println("First version (\"original\") set");

    seqMerger.addVersion(annaVersion, "anna");
    System.out.println("Version \"anna\" added");

    seqMerger.addVersion(benVersion, "ben");
    System.out.println("Version \"ben\" added");

    seqMerger.addVersion(chrisVersion, "chris");
    System.out.println("Version \"chris\" added");

    seqMerger.addVersion(davidVersion, "david");
    System.out.println("Version \"david\" added");

    seqMerger.setResultType(SequentialMerge.MergeResultType.DELTAV2);
    seqMerger.extractAll(new File("seq-merge-result-deltav2.xml"));
    seqMerger.setResultType(SequentialMerge.MergeResultType.ANALYZED_DELTAV2);
    seqMerger.extractAll(new File("seq-merge-result-analyzed.xml"));
    seqMerger.setResultType(SequentialMerge.MergeResultType.SIMPLIFIED_DELTAV2);
    seqMerger.extractAll(new File("seq-merge-result-simplified-delta.xml"));

  }
}